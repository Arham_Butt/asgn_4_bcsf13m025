#include<stdio.h>
#include<string.h>	//strlen
#include<sys/socket.h>
#include<arpa/inet.h>	//inet_addr
#include<unistd.h>	//write
#include <pwd.h>
#include <grp.h>
#include <sys/types.h>
#include <sys/stat.h>
#include<stdlib.h>
#include <dirent.h>
#include <errno.h>
#include<time.h>
#include <sys/ioctl.h>
#include<fcntl.h>

int main(int argc , char *argv[])
{
	int socket_desc , client_sock , c , read_size;
	struct sockaddr_in server , client;
	char client_message[2000];
	char temp[100];
	
	//Create socket
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1)
	{
		printf("Could not create socket");
	}
	puts("Socket created");
	
	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons( atoi(argv[1]) );
	
	//Bind
	if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		//print the error message
		perror("bind failed. Error");
		return 1;
	}
	puts("bind done");
	
	//Listen
	listen(socket_desc , 3);
	
	//Accept and incoming connection
	puts("Waiting for incoming connections...");
	c = sizeof(struct sockaddr_in);
	
	//accept connection from an incoming client
	client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
	if (client_sock < 0)
	{
		perror("accept failed");
		return 1;
	}
	puts("Connection accepted");
	int len=0;
	memset(client_message,'\0', 2000);
	memset(temp,'\0', 100);
	//Receive a message from client
	while( (read_size = recv(client_sock , client_message , 2000 , 0)) > 0 )
	{
		puts(client_message);
		if( client_message[0]=='l' && client_message[1] =='s' && client_message[2] =='\n'){
			//Send the message back to client
			DIR * dp= opendir(".");
			if(dp){
		
				struct dirent * entry;
				while ((entry=readdir(dp)) != NULL) {
					if ( !strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..") )
					{
					}
					 else {
					    
					
						for(int i=0; len< 2000 && i< strlen(entry->d_name) ; i++){
							client_message[len++] = entry->d_name[i];
						}
						client_message[len++] = ' ';
					}
				}
			}
		}
		if( client_message[0]=='c' && client_message[1] =='a' && client_message[2] =='t' && client_message[3] ==' '){		
			int j=4;
			char a;
			while( client_message[j] !='\n' ){
				temp[j-4] = client_message[j];				
				j++;
			}
			temp[j-4] = '\0';
			puts(temp);
			int fd = open(temp,O_RDONLY);
			memset(client_message,'\0', 2000);
			len=read(fd,client_message,2000);
			
			
	
		}
		if( client_message[0]=='c' && client_message[1] =='d' && client_message[2] ==' ' ){		
			int j=3;
			char a;
			while( client_message[j] !='\n' ){
				temp[j-3] = client_message[j];				
				j++;
			}
			temp[j-3] = '\0';
			puts(temp);
			int fd = chdir(temp);
			perror(" ");
			strcpy(client_message  ,"Directory changed");
			perror(" ");
			len=18;
			
	
		}
		client_message[len++] = '\0';					
		send(client_sock , client_message , strlen(client_message), 0);
		memset(client_message,'\0', 2000);
		memset(temp,'\0', 100);
		len=0;
	}
	
	if(read_size == 0)
	{
		puts("Client disconnected");
		fflush(stdout);
	}
	else if(read_size == -1)
	{
		perror("recv failed");
	}
	
	return 0;
}
